import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueToastr from 'vue-toastr'
import VueSweetalert2 from 'vue-sweetalert2'
import './registerServiceWorker'

Vue.use(VueSweetalert2)

Vue.use(VueToastr, {
  /* OverWrite Plugin Options if you need */
  defaultTimeout: 3000,
  defaultPosition: 'toast-bottom-left',
  defaultCloseOnHover: false
})

library.add(faArrowCircleLeft)

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
