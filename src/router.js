import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import MyPokemon from './views/MyPokemon.vue'
import Detail from './views/Detail.vue'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/detail/:id',
      name: 'detail',
      props: true,
      component: Detail
    },
    {
      path: '/my-pokemon',
      name: 'myPokemon',
      component: MyPokemon
    }
  ],
  mode: 'history'
})
