import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    apiBase: 'https://pokeapi.co/api/v2/pokemon',
    myPokemon: []
  },
  actions: {
    fetchData ({ commit }, payload) {
      return new Promise((resolve, reject) => {
        switch (payload.method) {
          case 'post':
            break
          case 'get':
            axios
              .get(payload.url)
              .then(res => {
                return res
              })
              .then(r => {
                resolve(r.data)
              })
              .catch(err => {
                this.$toastr.e(
                  err
                )
              })
            break
          default:
        }
      })
    }
  }
})
